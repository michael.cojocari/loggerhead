export class ContactDTO {
    readonly callsign: string;
    readonly frequency: string;
    readonly location: string;
    readonly notes: string;
}
