import * as mongoose from 'mongoose';

export const ContactSchema = new mongoose.Schema({
    callsign: String,
    frequency: String,
    location: String,
    notes: String,
});
