import { Module } from '@nestjs/common';
import { ContactService } from './contact.service';
import { ContactController } from './contact.controller';
import { DatabaseModule } from '../database/database.module';
import { contactProvider } from './contact.provider';

@Module({
    imports: [DatabaseModule],
    providers: [ContactService, ...contactProvider],
    controllers: [ContactController],
})
export class ContactModule {}
