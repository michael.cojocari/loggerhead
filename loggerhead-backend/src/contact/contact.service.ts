import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Contact } from './contact.interface';
import { ContactDTO } from './contact.dto';

@Injectable()
export class ContactService {
    constructor(@Inject('CONTACT_MODEL') private contactModel: Model<Contact>) {}

    async getContacts(): Promise<Contact[]> {
        return this.contactModel.find().exec();
    }

    async createContact(createContactDTO: ContactDTO): Promise<Contact> {
        return this.contactModel.create(createContactDTO);
    }
}
