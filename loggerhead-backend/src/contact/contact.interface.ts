import { Document } from 'mongoose';

export interface Contact extends Document {
    readonly callsign: string;
    readonly frequency: string;
    readonly location: string;
    readonly notes: string;
}
